# arista-cvp-configbk

Backup all switch configurations from CVP API; using Ansible arista.cvp Ansible galaxy collection


# How To Use

## clone repository

```bash
$ git clone https://gitlab.com/aristacurtis/arista-cvp-configbk.git
$ cd arista-cvp-configbk
```

## modify relevant variables
  - inventory.ini | `change ansible_host, ansible_user, ansible_password variables`
  - **if** using secrets.yml *_(encrypted vault)_*, create appropriate secrets.yml file and encrypt it:
    ```bash
    $ echo "vault_ansibleuser: \"cvpusername\"" > secrets.yml
    $ echo "vault_ansiblepass: \"cvppassword\"" >> secrets.yml
    $ ansible-vaule encrypt secrets.yml
    ```

## run it

```bash
ansible-playbook -i inventory.ini --ask-vault-pass playbook.factsgather.yml
```
### what it does

1.  uses _arista.cvp.cv_facts_ collection module to connect to CVP API and collect facts (container topology, devices, configlets, etc) from CVP
2.  uses CVP API to pull running-config from each device identified in step 1 (cv_facts)

## dependencies

This playbook requires the following to be installed on the Ansible control machine:

   -  python 3.7
   -  ansible >= 2.9.0
   -  requests >= 2.22.0
   -  treelib version 1.5.5
   -  jmespath >= 0.10.0
      * can be installed with:
```bash 
$ pip3 install -r requirements.txt
``` 
   - **NOTE: if SELinux enabled, uncomment selinux line from requirements.txt or install individually:
```bash
$ pip3 install selinux
```

Additionally, the <b>arista.cvp</b> collection needs to be installed from galaxy:

```bash
$ ansible-galaxy collection install arista.cvp:==1.1.0
```
